## Description

[Nest](https://github.com/nestjs/nest) NestJS with GraphQL and MongoDB boilerplate.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start
```

Navigate to http://localhost:3000/graphql
