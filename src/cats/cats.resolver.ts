import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { CatsService } from './cats.service';
import { CreateCatDto } from './dto/create-cat.dto';
import { Cat } from './schemas/cat.schema';
import { CatInput } from './inputs/cat.input';

@Resolver()
export class CatsResolver {
  constructor(private catService: CatsService) {}

  @Query(() => String)
  async hello(): Promise<string> {
    return 'hello';
  }

  @Query(() => [CreateCatDto])
  async cats(): Promise<Cat[]> {
    return this.catService.findAll();
  }
  @Mutation(() => CreateCatDto)
  async createCat(@Args('input') catInput: CatInput): Promise<Cat> {
    return this.catService.create(catInput);
  }
}
